#!/bin/bash

if [ -n "$ZOE_USER" ]; then
    cat /etc/sudoers | sed -E 's/Defaults[[:blank:]]+env_reset/Defaults !env_reset/' > /etc/sudoers.tmp
    mv /etc/sudoers.tmp /etc/sudoers
    echo "Creating $ZOE_USER user..."
    groupadd -g $ZOE_GID zoe-user
    useradd -s /bin/bash -N -u $ZOE_UID -d $ZOE_WORKSPACE -g $ZOE_GID $ZOE_USER
    cd $ZOE_WORKSPACE
    if [ -x /opt/custom_startup.sh ]; then
        /opt/custom_startup.sh
    fi
    sudo -E -H -u $ZOE_USER $@
else
    cd $ZOE_WORKSPACE
    export HOME=$ZOE_WORKSPACE
    if [ -x /opt/custom_startup.sh ]; then
        /opt/custom_startup.sh
    fi
    exec $@
fi

