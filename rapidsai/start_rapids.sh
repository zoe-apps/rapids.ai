#!/bin/bash

export PATH=$PATH:/conda/bin

cd /rapids
source activate gdf
cd /mnt/workspace
jupyter lab --no-browser --NotebookApp.token="${NOTEBOOK_PASSWORD}" --allow-root --ip=0.0.0.0 --NotebookApp.base_url=${REVERSE_PROXY_PATH_8888}

